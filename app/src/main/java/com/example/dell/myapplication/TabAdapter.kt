package com.example.dell.myapplication

/**
 * Created by Dell on 27/10/2018.
 */
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.dell.myapplication.Fragments.F1Fragment

class TabAdapter(fm: FragmentManager?, var fragments: ArrayList<Fragment>) : FragmentPagerAdapter(fm) {

    override fun getItem(p0: Int): Fragment {
        return fragments[p0]

    }

    override fun getCount(): Int {
        return fragments.size

    }
    override fun getPageTitle(position: Int): CharSequence? {

        return when (position) {
            0 ->
                "First"
            1 ->
                "Second "
            2 ->
                "Three "
            else ->
                "Three "

        }

    }}
