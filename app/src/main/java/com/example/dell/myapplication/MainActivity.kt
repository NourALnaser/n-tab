package com.example.dell.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import   com.example.dell.myapplication.Fragments.F1Fragment
import   com.example.dell.myapplication.Fragments.F2Fragment
import   com.example.dell.myapplication.Fragments.F3Fragment


import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val fragment = ArrayList<Fragment>()
        fragment.add(F1Fragment())
        fragment.add(F2Fragment())
        fragment.add(F3Fragment())


        var fragmentPagerAdapter = TabAdapter(supportFragmentManager, fragment)
        mViewPager.adapter = fragmentPagerAdapter

        mTabLayout.setupWithViewPager(mViewPager)


    }
}
